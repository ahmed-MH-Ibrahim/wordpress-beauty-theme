<?php 
/***
 * Comments Template
 *
 * This template displays the current comments of a post and the comment form
 *
 */

if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
	die ('Please do not load this page directly. Thanks!');

if ( post_password_required()) : ?>
	<p><?php _e('Enter password to view comments.', 'zeeDynamic_language'); ?></p>
<?php return; endif; ?>


<?php if ( have_comments() or comments_open() ) : ?>

		<div class="comments">
	
		<?php if ( have_comments() ) : ?>

			<div class="title clearfix">
				<div class="title-begin fl"></div>
				<h3 class="fl">Comments <?php //comments_number('', __('One comment','beauty_theme'), __('% comments','beauty_theme') ) ?></h3>
				<div class="title-end"></div>
			</div>
			<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
			<div class="comment-pagination clearfix">
				<div class="alignleft"><?php previous_comments_link(); ?></div>
				<div class="alignright"><?php next_comments_link() ?></div>
			</div>
			<?php endif; ?>
			
			<ul class="commentlist col-xs-12">
				<?php wp_list_comments( array('callback' => 'creative_comments')); ?>
			</ul>

			<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
			<div class="comment-pagination clearfix">
				<div class="alignleft"><?php previous_comments_link() ?></div>
				<div class="alignright"><?php next_comments_link() ?></div>
			</div>
			<?php endif; ?>
			
		<?php endif; ?>

		<?php if ( comments_open() ) : ?>
			<div class="reply-section col-xs-12">
				<?php //comment_form(array('comment_notes_after' => ''));
					$args = array(
					  'id_form'           => 'comment-form',
					  'class_form'	=> 'col-xs-12 comment-form',
					  'id_submit'         => 'submit',
					  'title_reply_before' => "<div class='reply-header col-xs-12'><h3>",
					  'title_reply'       => __( '' ),
					  'title_reply_after' => '</h3></div>',
					  'title_reply_to'    => __( 'Reply to %s' ),
					  'cancel_reply_link' => __( 'Cancel' ),
					  'label_submit'      => __( 'Post Comment' ),
					  'class_submit' => 'btn pink-btn',
					  'cancel_reply_before' => '<div class="cancel">',
					  'cancel_reply_after' => '</div>',

					  /*'comment_field' =>  '<p class="comment-form-comment"><label for="comment">' . _x( 'Comment', 'noun' ) .
					    '</label><textarea id="comment" name="comment" cols="30" rows="10" aria-required="true">' .
					    '</textarea></p>',*/
					   

					  'must_log_in' => '<p class="must-log-in">' .
					    sprintf(
					      __( 'You must be <a href="%s">logged in</a> to post a comment.' ),
					      wp_login_url( apply_filters( 'the_permalink', get_permalink() ) )
					    ) . '</p>',

					  /*'logged_in_as' => '<p class="logged-in-as">' .
					    sprintf(
					    __( 'Loggeddd in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>' ),
					      admin_url( 'profile.php' ),
					      $user_identity,
					      wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) )
					    ) . '</p>',*/
						'logged_in_as' => '',

					  /*'comment_notes_before' => '<p class="comment-notes">' .
					    __( 'Some comment Before input fields','creative-dir' ) . ( $req ? $required_text : '' ) .
					    '</p>',*/
					  

					  /*'comment_notes_after' => '<p class="form-allowed-tags">' .
					    sprintf(
					      __( 'You may use thesessss <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes: %s' ),
					      ' <code>' . allowed_tags() . '</code>'
					    ) . '</p>',*/
						

					  'fields' => apply_filters( 'comment_form_default_fields', array(

					    /*'author' =>
					      '<p class="comment-form-author">' .
					      '<label for="author">' . __( 'Name', 'domainreference' ) . '</label> ' .
					      ( $req ? '<span class="required">*</span>' : '' ) .
					      '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
					      '" size="30"' . $aria_req . ' /></p>',*/
					      'author' =>
					      '<p class="comment-form-author"> <span>Name 名字*</span><br/>' .
					      '<input id="author" name="author" type="text" ' .
					      '" size="30"' . $aria_req . ' /></p>',

					    /*'email' =>
					      '<p class="comment-form-email">' .
					      '<input id="email" name="email" type="text" value="Email' . esc_attr(  $commenter['comment_author_email'] ) .
					      '" size="30"' . $aria_req . ' /></p>',*/

					      'email' =>
					      '<p class="comment-form-email"> <span>Email 电邮地址*</span><br/>' .
					      '<input id="email" name="email" type="text" size="30"' . $aria_req . ' /></p>',

					      

					    /*'url' =>
					      '<p class="comment-form-url">' .
					      '<input id="url" name="url" type="text" onblur="if(this.value =='."' '".')this.value ='."'Website URL' ".'" onfocus="if(this.value =='."'Website URL'".')this.value ='."' ' ".'"  value="Website URL' . esc_attr( $commenter['comment_author_url'] ) .
					      '" size="30" /></p>'*/

					    )
					  ),
					'comment_notes_before' => '',
 					'comment_field' =>  '<span>Comment 评论*</span> <br/><textarea id="comment" name="comment" cols="30" rows="10" aria-required="true">' .
					    '</textarea>',

					'comment_notes_after' => '',
					);
					comment_form($args);
				 ?>
			</div>
		<?php endif; ?>

		</div>

<?php endif; ?>