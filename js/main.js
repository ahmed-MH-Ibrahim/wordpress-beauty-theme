
jQuery(document).ready(function($){
	$("#gototop").click(function(){
		$(window).scrollTop(0);
		$("#gototop").hide();
	});

	$(window).scroll(function(){
		if($(window).scrollTop() > 350)
		{
			$("#gototop").show(100);
		}
		else
		{
			$("#gototop").hide();
		}

		//sticky header check
		if($(window).scrollTop() > 450)
		{
			$("#sticky-main-navigation").show(100);
		}
		else
		{
			$("#sticky-main-navigation").hide();
		}
	});


	$(".toggled-menu, .menu-main-list").click(function(){
		$(".toggled-menu li").slideToggle("slow");
	});

	$(".toggled-menu-footer, .menu-main-list-footer").click(function(){
		$(".toggled-menu-footer li").slideToggle("slow");
	});

	//Gallery posts
	if($(window).width() <= 776)
	{
		$(".custom-gallery-nav").show();
	}

	$(window).resize(function(){
		if($(window).width() <= 776)
		{
			$(".custom-gallery-nav").show();
		}
		else
			$(".custom-gallery-nav").hide();
	});

	$(".gallery").hover(function(){
		$(".custom-gallery-nav").show();
	},function(){
		if($(window).width() > 776)
			$(".custom-gallery-nav").hide();
	});

	$(".custom-gallery-next").click(function(){
		var gallery = $(this).parent();
		var index = parseInt($(this).parent().attr("data-image-index"));
		var images_count = parseInt($(this).parent().attr("data-images-count"));

		if(index<images_count-1)
		{
			index++;
			var currentImage = $(this).parent().children(".show-gallery-item");

			currentImage.next().removeClass("hide-gallery-item").addClass("show-gallery-item");
			currentImage.removeClass("show-gallery-item").addClass("hide-gallery-item");

			$(this).parent().attr("data-image-index",index);
		}

	});

	$(".custom-gallery-prev").click(function(){
		var gallery = $(this).parent();
		var index = parseInt($(this).parent().attr("data-image-index"));
		var images_count = parseInt($(this).parent().attr("data-images-count"));

		if(index>=1)
		{
			index--;
			var currentImage = $(this).parent().children(".show-gallery-item");

			currentImage.prev().removeClass("hide-gallery-item").addClass("show-gallery-item");
			currentImage.removeClass("show-gallery-item").addClass("hide-gallery-item");

			$(this).parent().attr("data-image-index",index);
		}

	});

	$(".qr-display").click(function(){
		$(this).next().slideToggle("fast");
	})

	$(".gallery-item").hide();
	$(".gallery-item").parent().hide();

	$(".the_champ_horizontal_sharing").next().css("clear","none");

	$("input[name='es_txt_name_pg']").parent().prev().hide();
	$("input[name='es_txt_name_pg']").parent().hide();
	$("input[name='es_txt_email_pg']").parent().prev().hide();
	$("input[name='es_txt_email_pg']").attr("placeholder","Email");
	var heart = $("#s").attr("data-img-loc");
	/*$("input[name='es_txt_email_pg']").css({"background-image":"url("+heart+")",
		"background-position":"right","background-size":"15px 15px","background-repeat":"no-repeat","background-position-x":"98%"});*/

	$("div.es_textbox").addClass("form-field col-md-offset-0 col-md-12 col-xs-offset-1 col-xs-10");
	$("input[name='es_txt_email_pg']").css("float","left");
	$("input[name='es_txt_email_pg']").addClass("col-md-10 col-sm-10 col-xs-10");
	$("input[name='es_txt_email_pg']").attr("placeholder","Email subscription 订阅");
	$("input[name='es_txt_email_pg']").parent().append('<input type="image" id="sub_email_replaced_button"  width="24px" height="24px" style="padding:2px;border-left:0;float:left;" src="'+heart+'" alt="submit Button" onMouseOver="this.src='+heart+'">')
	$("#sub_email_replaced_button").click(function(){
		$("input[name='es_txt_button_pg']").trigger("click");
	});
	$("input[name='es_txt_button_pg']").hide();
	//$("input[name='es_txt_button_pg']").remove();
	//update like button
	var likeImg = $(".like-img-holder").attr("data-like-img-holder");
	$(".action-like a img").remove();
	$(".action-like a ").prepend('<img width="16px" height="13px" src="'+likeImg+'" title="Like">');
	$(".action-like a img").css("margin-top","3px");
	

});