<?php get_header();?>
	  	<section class="content col-md-8">
	  		<article class="col-xs-12 archives" >
		  		<?php 
		  			//add it before main loop
		  			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1; 
		  			query_posts(array(
						//'posts_per_page' => 5, // custom post type
				        'paged'=>$paged,
				    ));
				?>
		  		<?php if(have_posts()): while(have_posts()) :  the_post(); 
		  			$month = mysql2date('F Y', $post->post_date);
		  		 ?>

		  		<?php if($month != $prevmonth): $prevmonth = $month; ?>
		  			<div class="monthly-posts">
		  				<h3 class="title"><?php echo substr($month,0,3).mysql2date(' Y', $post->post_date); ?></h3>
		  			</div> 
		  		<?php endif;?>
		  			<p class="archives-posts"><a href="<?php the_permalink(); ?>"><img width="20px" src="<?php echo get_template_directory_uri().'/img/heart.png' ?>"> <?php the_title(); ?></a></p>

				<?php endwhile; else :?>  
					<h1 class="notfound"><?php _e("No Posts were found !","beauty-theme"); ?></h1>
				<?php endif; ?> 
			</article>
	  	</section>

	  	<?php include ('sidebar-archives.php'); ?>
	  </section>
	</div>
	<?php get_footer(); ?>