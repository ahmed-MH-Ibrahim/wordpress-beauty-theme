<?php get_header();?>
	  	<section class="content col-md-8">
	  		<?php if(have_posts()): while(have_posts()) : the_post(); ?>	

				<?php get_template_part('content', get_post_format()); // ex: Standard (no post format): content.php, Gallery:content-gallery.php?> 
				
			<?php endwhile; else :?>  
				<h1 class="notfound"><?php _e("No Posts were found !","beauty-theme"); ?></h1>
			<?php endif; ?>
	  		<!-- <article class="beauty">
	  			<div class="postinfo clearfix">	
					<span class="meta-category">
						<ul class="post-categories">
							<li><a href="http://www.war-bixin.com/category/business/"  rel="category tag">Beauty</a></li>
						</ul>		
					</span>	
				</div>

				<h2 class="post-title">The entire auction will end when there</h2>
				<div class="entry clearfix">
					<img src="<?php echo get_template_directory_uri();?>/img/el_sku_YTHK01_558x768_0.jpg" class="img-responsive">
					<p>Using our auction is very straightforward.  You can browse our current auction, entirely or choose to use our user-friendly navigation tools and search bar. Once you have selected an item and you are confident you wish to bid on the item it is very straightforward: Our auctions will end anytime from 7pm (GMT).  Once 7pm is reached activity on any item will prolong the ENTIRE auction by a further 2 minutes.  The entire auction will end when there has been no activity placed within the last two...</p>

					<a class="more-link fr" href="index.php">Keep reading ... <img src="img/beauty.png"></a>
					<div class="hr-styled"><img src="<?php echo get_template_directory_uri();?>/img/pink-row-bg.png"></div>
				</div>
				
	  		</article>
	  		

	  		<article class="fashion">
	  			<div class="postinfo clearfix">	
					<span class="meta-category">
						<ul class="post-categories">
							<li><a href="http://www.war-bixin.com/category/business/" rel="category tag">Fashion</a></li>
						</ul>		
					</span>	
				</div>

				<h2 class="post-title">The entire auction will end when there</h2>
				<div class="entry clearfix">
					<div><img src="<?php echo get_template_directory_uri();?>/img/jewellery_summer.jpg"></div>
					<p>Using our auction is very straightforward.  You can browse our current auction, entirely or choose to use our user-friendly navigation tools and search bar. Once you have selected an item and you are confident you wish to bid on the item it is very straightforward: Our auctions will end anytime from 7pm (GMT).  Once 7pm is reached activity on any item will prolong the ENTIRE auction by a further 2 minutes.  The entire auction will end when there has been no activity placed within the last two...</p>

					<a class="more-link fr" href="index.php">Keep reading ... <img src="<?php echo get_template_directory_uri();?>/img/beauty.png"></a>
					<div class="hr-styled"><img src="<?php echo get_template_directory_uri();?>/img/green-row-bg.jpg"></div>
				</div>
				
	  		</article>
	  		

	  		</article>

	  			<article class="travel">
	  			<div class="postinfo clearfix">	
					<span class="meta-category">
						<ul class="post-categories">
							<li><a href="http://www.war-bixin.com/category/business/"  rel="category tag">Travel</a></li>
						</ul>		
					</span>	
				</div>

				<h2 class="post-title">The entire auction will end when there</h2>
				<div class="entry clearfix">
					<p><img src="<?php echo get_template_directory_uri();?>/img/Prague.jpg"></p>
					<p>Using our auction is very straightforward.  You can browse our current auction, entirely or choose to use our user-friendly navigation tools and search bar. Once you have selected an item and you are confident you wish to bid on the item it is very straightforward: Our auctions will end anytime from 7pm (GMT).  Once 7pm is reached activity on any item will prolong the ENTIRE auction by a further 2 minutes.  The entire auction will end when there has been no activity placed within the last two...</p>

					<a class="more-link fr" href="index.php">Keep reading ... <img src="<?php echo get_template_directory_uri();?>/img/beauty.png"></a>
					<div class="hr-styled"><img src="<?php echo get_template_directory_uri();?>/img/blue-row-bg.jpg"></div>
				</div>
				
	  		</article>
	  		

	  		</article> -->
	  	</section>

	  	<?php get_sidebar(); ?>
	  </section>
	</div>
	<?php get_footer(); ?>