<!DOCTYPE HTML PUBLIC>
<html lang="en">
<head>
<meta name="author" content="Ahmed I.">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" >
<meta name="keywords" content="">
<title><?php wp_title('|',true,'right');?><?php bloginfo('name'); ?></title>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="icon" href="favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>">


<?php wp_head(); ?>
</head>
<body>
 <div id="gototop" title="Go to top"><img src="<?php echo get_template_directory_uri();?>/img/gototop.png" height="100px"></div>
 <div class="container">
 	<div class="row">
	  <header id="header" class="header col-md-offset-0 col-md-12 col-xs-offset-3 col-xs-6">
	  	<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="col-md-offset-4 col-md-4"><img src='<?php echo get_template_directory_uri() ."/img/Emma-logo.png"; ?>' alt="emma's cottage" width="100%"></a>
	  </header>
	</div>
	<div class="row">
	  <nav id="main-navigation" class="navigation col-sm-offset-0 col-sm-12  col-xs-12">
	  	<ul class="main-list">
	  		<li ><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><span class='en-wrap'><h5 class='en'>Home</h5></span><span class='cn-wrap'><h5 class='cn'>主页</h5></span></a></li>
	  		<li ><a href="<?php echo esc_url( home_url( '/about' ) ); ?>"><span  class='en-wrap'><h5 class='en'>About</h5></span><span class='cn-wrap'><h5 class='cn'>关于</h5></span></a></li>
	  		<li ><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><span class='en-wrap'><h5 class='en'>Blog</h5></span><span class='cn-wrap'><h5 class='cn'>博客</h5></span></a>
	  			<ul>
	  				<li><a href="<?php echo esc_url( home_url( '/category/beauty/' ) ); ?>"><h5>beauty</h5><img style="margin-top:10px;" src="<?php echo get_template_directory_uri();?>/img/beauty.png"></a></li>
	  				<li><a href="<?php echo esc_url( home_url( '/category/fashion/' ) ); ?>"><h5>fashion</h5><img style="margin-top:10px;" src="<?php echo get_template_directory_uri();?>/img/fashion.png"></a></li>
	  				<li><a href="<?php echo esc_url( home_url( '/category/travel/' ) ); ?>"><h5>travel</h5><img style="margin-top:10px;" src="<?php echo get_template_directory_uri();?>/img/menu_icon_395.png"></a></li>
	  				<li><a href="<?php echo esc_url( home_url( '/category/lifestyle/' ) ); ?>"><h5>lifestyle</h5><img style="margin-top:10px;" height="34px" src="<?php echo get_template_directory_uri();?>/img/menu_icon_396.png"></a></li>
	  			</ul>	
	  		</li>
	  		<li ><a href="<?php echo esc_url( home_url( '/archives' ) ); ?>"><span class='en-wrap'><h5 class='en'>Archives</h5></span><span class='cn-wrap'><h5 class='cn'>分类</h5></span></a></li>
	  		<li><a href="http://weidian.com/?userid=900374688&infoType=1"><span class='en-wrap'><h5 class='en'>Shop</h5></span><span class='cn-wrap'><h5 class='cn'>商店</h5></span></a></li>
	  	</ul>

	  	<ul class="menu-main-list">
	  		<div class="toggle-header">
	  			<h5>menu</h5>
	  			<img src="<?php echo get_template_directory_uri();?>/img/pages-toggle.png">
	  		</div>
	  		<div class="toggled-menu">
		  		<li class="col-xs-12"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><span class='en-wrap'><h5 class='en'>Home</h5></span><span class='cn-wrap'><h5 class='cn'>主页</h5></span></a></li>
		  		<li class="col-xs-12"><a href="<?php echo esc_url( home_url( '/about' ) ); ?>"><span  class='en-wrap'><h5 class='en'>About</h5></span><span class='cn-wrap'><h5 class='cn'>关于</h5></span></a></li>
		  		<li class="col-xs-12"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><span class='en-wrap'><h5 class='en'>Blog</h5></span><span class='cn-wrap'><h5 class='cn'>博客</h5></span></a>
		  			<div class-"row">
			  			<ul class="col-xs-offset-1 col-xs-10">
			  				<li><a href="<?php echo esc_url( home_url( '/category/beauty/' ) ); ?>"><h5>beauty</h5><img style="margin-top:10px;" src="<?php echo get_template_directory_uri();?>/img/beauty.png"></a></li>
	  				<li><a href="<?php echo esc_url( home_url( '/category/fashion/' ) ); ?>"><h5>fashion</h5><img style="margin-top:10px;" src="<?php echo get_template_directory_uri();?>/img/fashion.png"></a></li>
	  				<li><a href="<?php echo esc_url( home_url( '/category/travel/' ) ); ?>"><h5>travel</h5><img style="margin-top:10px;" src="<?php echo get_template_directory_uri();?>/img/menu_icon_395.png"></a></li>
	  				<li><a href="<?php echo esc_url( home_url( '/category/lifestyle/' ) ); ?>"><h5>lifestyle</h5><img style="margin-top:10px;" height="34px" src="<?php echo get_template_directory_uri();?>/img/menu_icon_396.png"></a></li>
			  			</ul>	
			  		</div>
		  		</li>
		  		<li class="col-xs-12"><a href="<?php echo esc_url( home_url( '/archives' ) ); ?>"><span class='en-wrap'><h5 class='en'>Archives</h5></span><span class='cn-wrap'><h5 class='cn'>分类</h5></span></a></li>
		  		<li class="col-xs-12"><a href="http://weidian.com/?userid=900374688&infoType=1"><span class='en-wrap'><h5 class='en'>Shop</h5></span><span class='cn-wrap'><h5 class='cn'>商店</h5></span></a></li>
		  	</div>
	  	</ul>
	  </nav>
	</div>

	<div class="row">
		<nav id="sticky-main-navigation" class="navigation col-sm-offset-0 col-sm-12  col-xs-12">
	  	<ul class="main-list">
	  		<li ><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><span class='en-wrap'><h5 class='en'>Home</h5></span><span class='cn-wrap'><h5 class='cn'>主页</h5></span></a></li>
	  		<li ><a href="<?php echo esc_url( home_url( '/about' ) ); ?>"><span  class='en-wrap'><h5 class='en'>About</h5></span><span class='cn-wrap'><h5 class='cn'>关于</h5></span></a></li>
	  		<li ><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><span class='en-wrap'><h5 class='en'>Blog</h5></span><span class='cn-wrap'><h5 class='cn'>博客</h5></span></a>
	  			<ul>
	  				<li><a href="<?php echo esc_url( home_url( '/category/beauty/' ) ); ?>"><h5>beauty</h5><img style="margin-top:10px;" src="<?php echo get_template_directory_uri();?>/img/beauty.png"></a></li>
	  				<li><a href="<?php echo esc_url( home_url( '/category/fashion/' ) ); ?>"><h5>fashion</h5><img style="margin-top:10px;" src="<?php echo get_template_directory_uri();?>/img/fashion.png"></a></li>
	  				<li><a href="<?php echo esc_url( home_url( '/category/travel/' ) ); ?>"><h5>travel</h5><img style="margin-top:10px;" src="<?php echo get_template_directory_uri();?>/img/menu_icon_395.png"></a></li>
	  				<li><a href="<?php echo esc_url( home_url( '/category/lifestyle/' ) ); ?>"><h5>lifestyle</h5><img style="margin-top:10px;" height="34px" src="<?php echo get_template_directory_uri();?>/img/menu_icon_396.png"></a></li>
	  			</ul>	
	  		</li>
	  		<li ><a href="<?php echo esc_url( home_url( '/archives' ) ); ?>"><span class='en-wrap'><h5 class='en'>Archives</h5></span><span class='cn-wrap'><h5 class='cn'>分类</h5></span></a></li>
	  		<li><a href="http://weidian.com/?userid=900374688&infoType=1"><span class='en-wrap'><h5 class='en'>Shop</h5></span><span class='cn-wrap'><h5 class='cn'>商店</h5></span></a></li>
	  	</ul>

	  	<ul class="menu-main-list">
	  		<div class="toggle-header">
	  			<h5>menu</h5>
	  			<img src="<?php echo get_template_directory_uri();?>/img/pages-toggle.png">
	  		</div>
	  		<div class="toggled-menu">
		  		<li class="col-xs-12"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><span class='en-wrap'><h5 class='en'>Home</h5></span><span class='cn-wrap'><h5 class='cn'>主页</h5></span></a></li>
		  		<li class="col-xs-12"><a href="<?php echo esc_url( home_url( '/about' ) ); ?>"><span  class='en-wrap'><h5 class='en'>About</h5></span><span class='cn-wrap'><h5 class='cn'>关于</h5></span></a></li>
		  		<li class="col-xs-12"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><span class='en-wrap'><h5 class='en'>Blog</h5></span><span class='cn-wrap'><h5 class='cn'>博客</h5></span></a>
		  			<div class-"row">
			  			<ul class="col-xs-offset-1 col-xs-10">
			  				<li><a href="<?php echo esc_url( home_url( '/category/beauty/' ) ); ?>"><h5>beauty</h5><img style="margin-top:10px;" src="<?php echo get_template_directory_uri();?>/img/beauty.png"></a></li>
	  				<li><a href="<?php echo esc_url( home_url( '/category/fashion/' ) ); ?>"><h5>fashion</h5><img style="margin-top:10px;" src="<?php echo get_template_directory_uri();?>/img/fashion.png"></a></li>
	  				<li><a href="<?php echo esc_url( home_url( '/category/travel/' ) ); ?>"><h5>travel</h5><img style="margin-top:10px;" src="<?php echo get_template_directory_uri();?>/img/menu_icon_395.png"></a></li>
	  				<li><a href="<?php echo esc_url( home_url( '/category/lifestyle/' ) ); ?>"><h5>lifestyle</h5><img style="margin-top:10px;" height="34px" src="<?php echo get_template_directory_uri();?>/img/menu_icon_396.png"></a></li>
			  			</ul>	
			  		</div>
		  		</li>
		  		<li class="col-xs-12"><a href="<?php echo esc_url( home_url( '/archives' ) ); ?>"><span class='en-wrap'><h5 class='en'>Archives</h5></span><span class='cn-wrap'><h5 class='cn'>分类</h5></span></a></li>
		  		<li class="col-xs-12"><a href="http://weidian.com/?userid=900374688&infoType=1"><span class='en-wrap'><h5 class='en'>Shop</h5></span><span class='cn-wrap'><h5 class='cn'>商店</h5></span></a></li>
		  	</div>
	  	</ul>
	  </nav>

	</div>


	<div class="row">
	  <section id="main-container" class="col-sm-12">
