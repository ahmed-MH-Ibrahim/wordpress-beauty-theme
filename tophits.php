<h3 class="widget_title">Top Hits Posts</h3>
	  			<div class="top-hits">
	  				<?php
	  				$popularpost = new WP_Query( array( 'posts_per_page' => 3, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ) );
						while ( $popularpost->have_posts() ) : $popularpost->the_post(); 
							//$media = get_attached_media( 'image' );
							$first_image = get_first_image_in_post($post->id); ?>
							 <?php //Get the content, apply filters and execute shortcodes
									$content = do_shortcode( apply_filters( 'the_content', $post->post_content ) );
									$embeds = get_media_embedded_in_content( $content );

									//$embeds is an array and each item is the HTML of an embedded media.
									//The first item of the array is the first embedded media in the content
									$first_video = $embeds[0];
							?> 
							<li class="top-post">
								<a class='post-thumb col-xs-12'  href="<?php the_permalink(); ?>">
									
									<?php echo"<br/>"; 
									if($first_image != null) 
										echo '<img src="'.$first_image.'" />'; 
									else if ($first_video != null)
										echo "<div class='embedded_sidebar_video'>$first_video</div>";
									?>
									<h4><?php the_title();?></h4> 
								</a>
								<div class="hr-styled"><img src="<?php echo get_template_directory_uri()."/img/pink-row-bg.png" ?>"></div>
							</li>
						<?php
						endwhile;
					?>

	  			</div>