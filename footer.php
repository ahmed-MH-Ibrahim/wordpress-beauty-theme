<div class="row">
		<footer id="footer" class=" col-sm-12">
			<div class="row">
				<nav id="footer-navigation" class="navigation col-sm-12">
					<ul class="main-list-footer col-md-offset-1 col-md-10 col-sm-12">
						<li ><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><span class='en-wrap'><h5 class='en'>Home</h5></span><span class='cn-wrap'><h5 class='cn'>主页</h5></span></a></li>
				  		<li ><a href="<?php echo esc_url( home_url( '/about' ) ); ?>"><span  class='en-wrap'><h5 class='en'>About</h5></span><span class='cn-wrap'><h5 class='cn'>关于</h5></span></a></li>
				  		<li ><a href="<?php echo esc_url( home_url( '/contact' ) ); ?>"><span class='en-wrap'><h5 class='en'>Contact</h5></span><span class='cn-wrap'><h5 class='cn'>联络</h5></span></a></li>
				  		<li ><a href="<?php echo esc_url( home_url( '/archives' ) ); ?>"><span class='en-wrap'><h5 class='en'>Archives</h5></span><span class='cn-wrap'><h5 class='cn'>分类</h5></span></a></li>
				  		<li ><a href="<?php echo esc_url( home_url( '/disclaimer' ) ); ?>"><span class='en-wrap'><h5 class='en'>Disclaimer</h5></span><span class='cn-wrap'><h5 class='cn'>声明</h5></span></a></li>
				  	</ul>

				  	<ul class="menu-main-list-footer">
				  		<div class="toggle-header-footer">
				  			<h5>menu</h5>
				  			<img src="<?php echo get_template_directory_uri();?>/img/pages-toggle.png">
				  		</div>
				  		<div class="toggled-menu-footer">
					  		<li ><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><span class='en-wrap'><h5 class='en'>Home</h5></span><span class='cn-wrap'><h5 class='cn'>主页</h5></span></a></li>
					  		<li ><a href="<?php echo esc_url( home_url( '/about' ) ); ?>"><span  class='en-wrap'><h5 class='en'>About</h5></span><span class='cn-wrap'><h5 class='cn'>关于</h5></span></a></li>
					  		<li ><a href="<?php echo esc_url( home_url( '/contact-us' ) ); ?>"><span class='en-wrap'><h5 class='en'>Contact</h5></span><span class='cn-wrap'><h5 class='cn'>联络</h5></span></a></li>
					  		<li ><a href="<?php echo esc_url( home_url( '/archives' ) ); ?>"><span class='en-wrap'><h5 class='en'>Archives</h5></span><span class='cn-wrap'><h5 class='cn'>分类</h5></span></a></li>
					  		<li ><a href="<?php echo esc_url( home_url( '/disclaimer' ) ); ?>"><span class='en-wrap'><h5 class='en'>Disclaimer</h5></span><span class='cn-wrap'><h5 class='cn'>声明</h5></span></a></li>
					  	</div>
				  	</ul>

			  	</nav>

			  	

		  	</div>
			<div class="copyrights-wrapper">
				<h4>COPYRIGHT @ EMMA'S COTTAGE. DESIGN BY JACKI LITTLE.</h4>
			</div>
		</footer>
	</div>
  </div>

  <div class="bg-lower-image"><img src="<?php echo get_template_directory_uri();?>/img/Emma-bottom.png" width="100%"></div>
  <?php wp_footer(); ?>
</body>
</html>
   