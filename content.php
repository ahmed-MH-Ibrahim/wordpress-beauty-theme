
<?php 

//set post data based on category
$article = [];
$categoryFound = false;

if(has_category('Fashion') || has_category('fashion'))
{
	$article = [ 'class' => 'fashion' , 'readMoreImg' =>  get_template_directory_uri()."/img/fashion.png", 'hr' => get_template_directory_uri()."/img/green-row-bg.png"];
	$categoryFound = true;
}
else if(has_category('Travel') || has_category('travel'))
{
	$article = [ 'class' => 'travel', 'readMoreImg' =>  get_template_directory_uri()."/img/travel.png", 'hr' => get_template_directory_uri()."/img/blue-row-bg.png"];
	$categoryFound = true;
}
else if(has_category('Lifestyle') || has_category('lifestyle'))
{
	$article = [ 'class' => 'fashion', 'readMoreImg' =>  get_template_directory_uri()."/img/menu_icon_396.png", 'hr' => get_template_directory_uri()."/img/green-row-bg.png"];
	$categoryFound = true;
}
else{
	$article = [ 'class' => 'beauty', 'readMoreImg' =>  get_template_directory_uri()."/img/beauty.png", 'hr' => get_template_directory_uri()."/img/pink-row-bg.png"];
	$categoryFound = true;
}

?>

<article <?php is_email_page() ? post_class($article['class']." col-xs-12 theme-email-content"):post_class($article['class']." col-xs-12 "); ?> id="post-<?php the_ID(); ?>">
	<div class="postinfo clearfix">	
		<span class="meta-category">
			<ul class="post-categories">
				<?php foreach(get_the_category() as $category): ?>
					<?php $category_link = get_category_link( $category->cat_ID ); ?>
					<li><a href="<?php echo esc_url( $category_link ); ?>"  rel="category tag"><?php echo $category->name; ?></a></li>
				<?php endforeach; ?>
			</ul>		
		</span>	
	</div>

	<h3 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
	<h5 class="post-date"><?php the_time( get_option( 'date_format' ) ); ?></h5>
	<div class="entry clearfix">
		<?php the_content(__("Keep reading ... <img src='".$article['readMoreImg']."'>","creative-dir")); ?>
		<div class="like-img-holder" data-like-img-holder="<?php echo get_template_directory_uri(); ?>/img/heart-wt.png"></div>

		<?php //$Path=$_SERVER['REQUEST_URI']; echo $Path; ?>
		<div class="post-icons fl">
			<?php //echo open_social_share_html_custom(get_permalink());?>

			<!-- <div class="list weibo"><a href=""><?php// echo custom_share(get_permalink()); ?><img width="40px" src="<?php echo get_template_directory_uri()."/img/chat-2.png" ?>"></a></div> -->
			<div class="list mail"><?php if(function_exists('email_link')) { email_link(); } ?></div>
			<div class="list qr">

				<!-- <a  style="cursor:pointer;" data-toggle="modal" data-target="#qr<?php the_ID(); ?>"> <img src="<?php echo get_template_directory_uri()."/img/chat.png" ?>"></a> -->
				<a  style="cursor:pointer;" class="qr-display"> <img width="40px" src="<?php echo get_template_directory_uri()."/img/chat.png" ?>"></a>
				<div class="qr-code qr-code-<?php the_ID(); ?>"><?php echo qrcodeCustom("qr-code-$post->ID",get_permalink()); ?></div>
				
			</div>
		</div>
		<!-- <a class="more-link fr" href="index.php">Keep reading ... <img src="<?php echo $article->readMoreImg ?>"></a> -->
		<div class="hr-styled"><img src="<?php echo $article['hr'] ?>"></div>
	</div>

</article>