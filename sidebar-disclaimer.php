<section class="sidebar col-md-4">
	  		<aside class="profile-picture col-xs-12">
	  			<h2 class="title"></h2>
	  			<img src="<?php echo get_theme_mod('disclaimer_profile_picture_setting',get_bloginfo('template_directory').'/img/3yxf014bE-impressionism-garden.JPG') ;?>" class="round-img">
	  		</aside>
	  		<aside class="col-xs-12">
	  			<?php include("shop.php"); ?>
	  		</aside>
	  		<aside class="col-xs-12">
	  			<?php include("placeholder.php"); ?>
	  		</aside>

	  		<aside class="col-xs-12 aside-form">
	  			<?php get_search_form() ?>
	  		</aside>

	  		<aside class="col-xs-12 aside-form">
	  			<?php include ('email-subscription.php'); ?>
	  		</aside>

	  		<aside class="col-xs-12">
	  			<img src="<?php echo get_template_directory_uri();?>/img/social-media.png" class="">
	  		</aside>
	  		<aside class="col-xs-12">
	  			<?php include("socialmedia.php"); ?>
	  		</aside>
	  		<aside class="col-xs-12">
	  			<h3 class="widget_title"><img src="<?php echo get_template_directory_uri(); ?>/img/instagram.png" style="margin-right:15px">Instagram</h3>
	  			<div class="intagram-feed">
	  				<?php echo display_instagram(array(), null); ?>
	  			</div>	
	  		</aside>
	  		<aside class="col-xs-12 tophits">
	  			<?php include("tophits.php"); ?>
	  		</aside>
	  	</section>
