<section class="sidebar col-md-4">
	  		<aside class="profile-picture col-xs-12">
	  			<h2 class="title">艾玛小屋</h2>
	  			<img src="<?php echo get_theme_mod('main_profile_picture_setting',get_bloginfo('template_directory').'/img/3yxf014bE-impressionism-garden.JPG') ;?>" class="round-img">
	  		</aside>

	  		<aside class="col-xs-12">
	  			<section class="categories col-xs-12">
		  			<h3 class="widget_title">Categories</h3>
		  			<div class="cat-row clearfix beauty col-xs-offset-1 col-xs-10"><h4><a href="<?php echo esc_url( home_url( '/category/beauty/' ) ); ?>">Beauty</a></h4><img style="margin-top:10px;" src="<?php echo get_template_directory_uri(); ?>/img/beauty.png"></div>
		  			<div class="cat-row clearfix fashion col-xs-offset-1 col-xs-10"><a href="<?php echo esc_url( home_url( '/category/fashion/' ) ); ?>"><h4>Fashion</a></h4><img style="margin-top:10px;" src="<?php echo get_template_directory_uri(); ?>/img/fashion.png"></div>
		  			<div class="cat-row clearfix travel col-xs-offset-1 col-xs-10"><a href="<?php echo esc_url( home_url( '/category/travel/' ) ); ?>"><h4>Travel</a></h4><img style="margin-top:10px;" src="<?php echo get_template_directory_uri(); ?>/img/travel.png"></div>
		  			<div class="cat-row clearfix lifestyle col-xs-offset-1 col-xs-10"><a href="<?php echo esc_url( home_url( '/category/lifestyle/' ) ); ?>"><h4>Lifestyle</a></h4><img style="margin-top:10px;" src="<?php echo get_template_directory_uri(); ?>/img/menu_icon_396.png"></div>
	  			</section>
	  		</aside>

	  		<aside class="col-xs-12 ">
	  			<section class="archives">
		  			<h3 class="widget_title">Archives</h3>
		  			<div class="archives-div">
		  				<?php 
				  			//add it before main loop
				  			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1; query_posts(array(
								//'posts_per_page' => 5, // custom post type
						        'paged'=>$paged,
						    ));
						?>
				  		<?php if(have_posts()): while(have_posts()) :  the_post(); 
				  			$month = mysql2date('F Y', $post->post_date);
				  			$archive_year  = get_the_time('Y');
							$archive_month = get_the_time('m'); 
				  		 ?>

				  		<?php if($month != $prevmonth): $prevmonth = $month; ?>
				  			<div class="monthly-posts">
				  				<p class="archives-posts"><a href="<?php echo get_month_link( $archive_year, $archive_month ); ?> "><?php echo $month;  ?></a></p>
				  			</div> 
				  		<?php endif;?>
				  			
						<?php endwhile; else :?>  
							<h1><?php _e("No Posts were found !","beauty-theme"); ?></h1>
						<?php endif; ?> 
		  			</div>
		  		</section>
	  		</aside>

	  		<aside class="col-xs-12">
	  			<?php include("shop.php"); ?>
	  		</aside>
	  		<aside class="col-xs-12">
	  			<?php include("placeholder.php"); ?>
	  		</aside>

	  		<aside class="col-xs-12 aside-form">
	  			<?php get_search_form() ?>
	  		</aside>

	  		<aside class="col-xs-12 aside-form">
	  			<?php include ('email-subscription.php'); ?>
	  		</aside>

	  		<aside class="col-xs-12">
	  			<img src="<?php echo get_template_directory_uri();?>/img/social-media.png" class="">
	  		</aside>
	  		<aside class="col-xs-12">
	  			<?php include("socialmedia.php"); ?>
	  		</aside>
	  		<aside class="col-xs-12">
	  			<h3 class="widget_title"><img src="<?php echo get_template_directory_uri(); ?>/img/instagram.png" style="margin-right:15px">Instagram</h3>
	  			<div class="intagram-feed">
	  				<?php echo display_instagram(array(), null); ?>
	  			</div>	
	  		</aside>
	  		<aside class="col-xs-12 tophits">
	  			<?php include("tophits.php"); ?>
	  		</aside>

	  		
	  	</section>
