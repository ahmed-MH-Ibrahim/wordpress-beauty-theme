<?php get_header();?>
	  	<section class="content col-md-8">
	  		<?php if(have_posts()): while(have_posts()) : the_post(); ?>	

				<?php get_template_part('content', get_post_format()); // ex: Standard (no post format): content.php, Gallery:content-gallery.php?> 
				
			<?php endwhile; else :?>  
				<h1 class="notfound"><?php _e("No Posts were found !","beauty-theme"); ?></h1>
			<?php endif; ?>


			<!-- Comments Area -->	
			<?php 
				if(!is_email_page())
					comments_template('',true); //loads comment.php 
				else
				{
					?>
					<script type="text/javascript">
						jQuery(document).ready(function($){
							$(".post-icons").hide();
							$("label[for='friendemail']").text("Email address");
							$("#wp-email-required").remove();
							$("#wp-email-button input[type='button']").val("Send");
						});
					</script>
					<?php
				}

			 ?>

	  	</section>

	  	<?php get_sidebar(); ?>
	  </section>
	</div>
	<?php get_footer(); ?>