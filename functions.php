<?php

define('directory', get_bloginfo('template_directory')); 
define('images', get_template_directory_uri()  ."/img");


function js_scripts() {
	wp_enqueue_style("bootstrapcss","https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css");
	wp_enqueue_script('bootstrap', "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js", array('jquery'));
	wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array('jquery'),false );
}

add_action( 'wp_enqueue_scripts', 'js_scripts' );

/* ---------- Post Formats ---------- */
if ( function_exists( 'add_theme_support' ) ){
	add_theme_support( 'post-formats', array('gallery') );
} 


// unregister all widgets 
function unregister_default_widgets() {  
    unregister_widget('WP_Widget_Pages'); 
    unregister_widget('WP_Widget_Calendar');
    unregister_widget('WP_Widget_Archives');
    unregister_widget('WP_Widget_Links');
    unregister_widget('WP_Widget_Meta'); 
    unregister_widget('WP_Widget_Search'); 
    unregister_widget('WP_Widget_Text'); 
    unregister_widget('WP_Widget_Categories');
    unregister_widget('WP_Widget_Recent_Posts');  
    unregister_widget('WP_Widget_Recent_Comments');
    unregister_widget('WP_Widget_RSS');
    unregister_widget('WP_Widget_Tag_Cloud');
    unregister_widget('WP_Nav_Menu_Widget');
    unregister_widget('Twenty_Eleven_Ephemera_Widget');
} 

//register sidebar per page to add widgets to
if(function_exists('register_sidebar')){
	

	register_sidebar(array(
						'name' => __('Sponsors Logo','widget-dir'),
						'id' => 'sponsor_logo',
						'description' => __('This is the sponsor logo widget to easily update event sponsors ','widget-dir'),
						'before_widget' => '<div class="sponsor col-md-4 col-sm-6 col-xs-12">',
						'after_widget' => '</div>',
						'before_title' => '',
						'after_title' => '',
						'before_image' => '<img src="',
						'after_image' => '" width="200px">',
	));
}


//comments section handler
function creative_comments($comment, $args, $depth){ //called for each comment
  	$GLOBALS['comment'] = $comment;
  	if(get_comment_type() == 'pingback' || get_comment_type() == 'trackback'): ?>
  	 	<li id="comment-<?php comment_ID(); ?>">
		  	<div <?php comment_class('comment-container clearfix') ?> >
		  		<div class="comment-content fr" id="nested">
		  			<div class="comment-meta-data clearfix">
			  			<?php comment_reply_link(array_merge($args, array('before' => "<div class='fr' href=''>", 'after' => "</div>" ,'depth' => $depth))); ?>
			  			<h4 class="fl"><?php _e("Pingback:","creative-dir"); ?></h4>
			  			<label class="fl"><?php edit_comment_link(); //An admin could edit this ?></label>
			  		</div>
			  		<?php 
			  			comment_author_link();
			  		?>	  			
		  		</div>
		  	</div> 
  	<?php elseif(get_comment_type() == 'comment') : ?>
	  	<li id="comment-<?php comment_ID(); ?>">
		  	<div <?php $comment->comment_parent !=0 ? comment_class('comment-container clearfix col-xs-offset-1 col-xs-11'):comment_class('comment-container clearfix col-xs-12') ?> >

		  		<div class="comment-image col-xs-2">
		  			<?php
		  				$avatar_size = 50;  // 50x50 image
		  				if($comment->comment_parent !=0){ // if a parent exists therefor this is a reply (nested)
		  					//$avatar_size = 40;  // no need to cange size in this case
		  				}

		  				echo get_avatar($comment, $avatar_size);
		  			?>  
		  			<h4 class="user"><?php comment_author_link(); ?></h4>
		  		</div> 
		  		<div class="comment-content col-xs-10">
	  				<div class="row">
		  				<label class="date col-xs-12">				
			  				<?php comment_date(); ?> <?php comment_time(); ?>
		  				</label>
	  				</div>
	  				
		  			<div class="row">
			  		<?php // Awating moderation section
			  			if($comment->comment_approved == '0'): //comment not approved yet 
			  		?>

			  				<p class="col-xs-12 notice"><?php _e("Your comment is awaiting moderation","creative-dir"); ?></p>
			  			<?php endif;?>

			  			
			  			<div class="col-xs-12">
				  			<?php
				  				//Actual comment text
				  				comment_text();
				  			?>
				  		</div>
				  	</div>

				  	<div class="row reply">
		  				<?php comment_reply_link(array_merge($args, array('before' => "<div class='col-xs-12' href=''>", 'after' => "</div>" ,'depth' => $depth))); ?>
		  			</div>
			  		
		  			
		  		</div>
		  		
		  	</div> 
	  	<!-- Notice last list item "</li>" is deleted since wordpress will added automatically -->
  	<?php endif;
  } 


  //move comment textarea to the bottom
  function wpb_move_comment_field_to_bottom( $fields ) {
	$comment_field = $fields['comment'];
	unset( $fields['comment'] );
	$fields['comment'] = $comment_field;
	return $fields;
}

add_filter( 'comment_form_fields', 'wpb_move_comment_field_to_bottom' );


//page count field ( cusotm field to database)
function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

// tracking views by addin it to header
function wpb_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;    
    }
    wpb_set_post_views($post_id);
}
add_action( 'wp_head', 'wpb_track_post_views');

function get_first_image_in_post($post_id) {
  global $post, $posts;
  $first_img = '';
  ob_start();
  ob_end_clean();

  //return first image from gallery
  if(get_post_format( $post_id ) == "gallery")
  {
  	$gallery_imgs= get_post_gallery_images( $post );
  	$first_img = $gallery_imgs[0];
  }
  else{
	  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
	  $first_img = $matches [1] [0];
	}


  if(empty($first_img)){ //Defines a default image
    $first_img = null;
  }
  return $first_img;
}


//Theme custom controls
//controls
function theme_customize_register( $wp_customize ) {
   	//All our sections, settings, and controls will be added here
	//Group
	$wp_customize->add_section( 'event_section' , array(
    'title'      => __( 'Beauty Theme Settings', 'mytheme' ),
    'priority'   => 1, //order in the list of sections
	) );

	//edit logo
	$wp_customize->add_setting( 'main_profile_picture_setting' , array(
	    'default'     => get_bloginfo('template_directory')."/img/3yxf014bE-impressionism-garden.png",
	    'transport'   => 'refresh', //how preview will be updated
	) );

	$wp_customize->add_control(
       new WP_Customize_Image_Control(
           $wp_customize,
           'logo',
           array(
               'label'      => __( 'Change main profile picture', 'theme_name' ),
               'section'    => 'event_section',
               'settings'   => 'main_profile_picture_setting',
               'context'    => 'your_setting_context' 
           )
       )
   );

	//edit logo
	$wp_customize->add_setting( 'about_profile_picture_setting' , array(
	    'default'     => get_bloginfo('template_directory')."/img/3yxf014bE-impressionism-garden.png",
	    'transport'   => 'refresh', //how preview will be updated
	) );

	$wp_customize->add_control(
       new WP_Customize_Image_Control(
           $wp_customize,
           'about_control',
           array(
               'label'      => __( 'Change about profile picture', 'theme_name' ),
               'section'    => 'event_section',
               'settings'   => 'about_profile_picture_setting',
               'context'    => 'your_setting_context' 
           )
       )
   );

	//edit logo
	$wp_customize->add_setting( 'disclaimer_profile_picture_setting' , array(
	    'default'     => get_bloginfo('template_directory')."/img/3yxf014bE-impressionism-garden.png",
	    'transport'   => 'refresh', //how preview will be updated
	) );

	$wp_customize->add_control(
       new WP_Customize_Image_Control(
           $wp_customize,
           'disclaimer_control',
           array(
               'label'      => __( 'Change disclaimer profile picture', 'theme_name' ),
               'section'    => 'event_section',
               'settings'   => 'about_profile_picture_setting',
               'context'    => 'your_setting_context' 
           )
       )
   );

	//edit logo
	$wp_customize->add_setting( 'contact_profile_picture_setting' , array(
	    'default'     => get_bloginfo('template_directory')."/img/3yxf014bE-impressionism-garden.png",
	    'transport'   => 'refresh', //how preview will be updated
	) );

	$wp_customize->add_control(
       new WP_Customize_Image_Control(
           $wp_customize,
           'contact_control',
           array(
               'label'      => __( 'Change contact profile picture', 'theme_name' ),
               'section'    => 'event_section',
               'settings'   => 'about_profile_picture_setting',
               'context'    => 'your_setting_context' 
           )
       )
   );

	$wp_customize->add_setting( 'shop_setting' , array(
	    'default'     => get_bloginfo('template_directory')."/img/shop-online.png",
	    'transport'   => 'refresh', //how preview will be updated
	) );

	$wp_customize->add_control(
       new WP_Customize_Image_Control(
           $wp_customize,
           'shop_image_control',
           array(
               'label'      => __( 'Change shop picture', 'theme_name' ),
               'section'    => 'event_section',
               'settings'   => 'shop_setting',
               'context'    => 'your_setting_context' 
           )
       )
   );

	$wp_customize->add_setting( 'placholder_setting' , array(
	    'default'     => get_bloginfo('template_directory')."/img/placeholder.png",
	    'transport'   => 'refresh', //how preview will be updated
	) );

	$wp_customize->add_control(
       new WP_Customize_Image_Control(
           $wp_customize,
           'placeholder_image_control',
           array(
               'label'      => __( 'Change placeholder picture', 'theme_name' ),
               'section'    => 'event_section',
               'settings'   => 'placholder_setting',
               'context'    => 'your_setting_context' 
           )
       )
   );

}

add_action( 'customize_register', 'theme_customize_register' );

function is_email_page()
{
	$emailPage = false;
	$url = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
	$pathFragments = explode('/', $url);
	$end = end($pathFragments);
	if( (count($pathFragments) > 0 && $pathFragments[count($pathFragments)-1] == "email")
	 || (count($pathFragments) > 1 && $pathFragments[count($pathFragments)-1] == '' && $pathFragments[count($pathFragments)-2] == "email"))
		$emailPage = true;

	return $emailPage;
}

?>