<?php get_header();?>
	  	<section class="content col-md-8">
	  		<?php if(have_posts()): while(have_posts()) : the_post(); ?>	

				<?php get_template_part('content', get_post_format()); // ex: Standard (no post format): content.php, Gallery:content-gallery.php?> 
				
			<?php endwhile; else :?>  
				<h1 class="notfound"><?php _e("No Posts were found !","beauty-theme"); ?></h1>
			<?php endif; ?>


			<!-- Comments Area -->	
			<?php comments_template('',true); //loads comment.php ?>
			<script type="text/javascript">
				jQuery(document).ready(function($){
					$(".post-icons, .the_champ_sharing_container , .the_champ_horizontal_sharing").hide();
				});
			</script>

	  	</section>

	  	<?php include ('sidebar-disclaimer.php'); ?>
	  </section>
	</div>
	<?php get_footer(); ?>